/**
 * @name AddressBook
 * @description Manages contacts and groups. This class also manages communication with the data manager.
 *  An address can contain up to 5 lines of text. The last line is always the name of the country.
 */

    package app;
 public class Address {

     /**
      * Attributes
      */
     private String street;
     private String Town;
     private String country;
     private String address;

     /**
     * The constructor accepts a string containing the parts of the address separated by semicolons.
     * Example: “1 Any Street;Any Town;;;Jamaica”
     */
     public Address(String address) {

         this.address = address;
         String[] street = this.address.split("[;]+");
         this.street = street[0];
         this.Town = street[1];
         this.country = street[2];
     }


      /**
       * Methods
       */

      /**
       * @name getCountry
       * @description
       *
       * @return String
       * @param
       */

       public String getCountry() {
         return this.country;
       }

       /**
        * @name getAddress
        */

        //Returns an array of the lines in an address not including blank lines. The last element in the array is the country.
       public String[] getAddress() {
          String[] address = new String[]{this.street, this.Town, this.country};
         return address;
       }

       //Returns a string with the address lines separated by newlines. Blank lines should be suppressed (i.e. not included)
      public String toString() {
         return getAddress()[0] + " " + getAddress()[1] + " " + getAddress()[2];
      }


    /**
     * Test Class
     * @param args
     */
    public static void main(String[] args)  {
        System.out.println("Hello Java");
        Address Van = new Address("1 Any Street;Any Town;;;Jamaica");
        System.out.print(Van.toString());

    }



 }
