package app;


/**
 * Class: Name
 * Description: Manages name information.
 * Constructor The constructor accepts first name and last name as parameters and
 * sets the relevant attributes accordingly.
 * Method Description
 * getFirstName Returns the first name.
 * getLastName Returns the last name.
 * changeLastName Change the last name to parameter value.
 */

public class Name {

    private String firstName;
    private String lastName;

    public Name(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String changeLastName(String newName) {
        return this.lastName = newName;
    }

    public static void main(String[] args)  {
        Name name = new Name("Vantol", "Bennett");
        System.out.println(name.getFirstName());
        System.out.println(name.getLastName());
        System.out.println(name.changeLastName("Toys"));
    }

}
