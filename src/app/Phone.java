package app;

import java.util.Objects;

/**
 * Class: Phone
 * Description: Manages phone data.
 */
public class Phone {

    private int phoneNumber;
    private String type;
    private int areaCode = 876;

    /**
     * Constructor              The constructor a 10 digit phone number, and type
     *                          (H=Home,W=Work,M=Mobile) as its parameters.
     *
     * Method                       Description
     * getAreaCode              Returns the area code portion of a phone number. The area code of a
     *                          10 digit phone number is the first three digits of the number.
     * getType                  Returns the type of number (see constructor)
     * getNumber                Returns the 10 digit phone number.
     * toString                 Returns a string formatted as “(876) 555-1234”.
     */


    public Phone(int phoneNumber, char type) {

        this.phoneNumber = phoneNumber;

        /* TODO: Try using a a Switch statement here
        * FIXME: The getType method return type is char fix the implementation used
        *
        * */
        if (Objects.equals(type, 'H')) {
            this.type = "Home";
        } else if (Objects.equals(type, 'W')) {
            this.type = "Work";
        } else if (Objects.equals(type, 'M')){
            this.type = "Mobile";
        }
    }

    public int getAreaCode() {
        return areaCode;
    }

    public String getType() {
        return type;
    }

    private int getNumber() {
        return phoneNumber;
    }

    /* FIXME: String conversion */
    public String toString() {

        String s = String.valueOf(getAreaCode());
        String a = String.valueOf(getNumber());
        final String s1 = s + a;
        return s1;
    }

    public static void main(String[] args)  {
        System.out.println("Hello Java");
        Phone phone = new Phone(3522225, 'W');
        System.out.print(phone.phoneNumber);
        System.out.print(phone.getType());

    }
}
