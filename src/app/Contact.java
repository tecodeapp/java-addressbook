package app;

/**
 * Class: Contact
 * Description: Manages contact data.
 * Constructor: The constructor accepts first name, last name, gender, and date of
 * birth as parameters. Generates a unique identifier (entry number) for
 * each contact. Gender is a string “Male”, or “Female”. Date of birth is a
 * number representing yyyymmdd. For example July 8, 2011 would be
 * represented by the number 20110708.
 * Method Description
 * getEntryNo Return the entry number for the contact. The entry number is
 * automatically generated when the Contact object is created as a serial
 * number (starting value 1).
 * getAge Return the contact’s age.
 * getName Return the contact’s name in the form “Brown, John”.
 * updateName Update last name of contact
 * getAlias Return the contact’s alias.
 * setAlias Sets the contact’s alias. An alias must be unique in the address book.
 * That is, no two contacts should have the same alias.
 * getAddress Return an array containing the lines in an address not including blank
 * lines. The last element in the array is the country.
 * setAddress Accepts a string with the parts of the address separated by semicolons
 * (see details on Address class).
 * addEmail Add an email address to a set of email address for the contact. Each
 * contact can have an infinite number of email addresses.
 * deleteEmail Delete the email address given as a parameter from the set of email
 * addresses for the contact.
 *
 * getEmailList Return an array of the email addresses for the contact.
 * addPhone Add a phone number to the list of phone numbers that the contact
 * has. The method accepts two parameters. The first is for the type of
 * number (H=Home, W=Work, M=Mobile). The second parameter is the
 * phone number where the first three digits are the area code. For
 * example, 8767024455. A contact can have up to 5 phone numbers.
 * deletePhone Delete the phone number given as a parameter from the list of phone
 * numbers.
 * getPhoneList Return an array of strings representing the contact’s phone numbers.
 * Each phone number is formatted as specified by the toString method
 * of the Phone class.
 */
public class Contact {


    /**
     * Constructor: The constructor accepts first name, last name, gender, and date of
     * birth as parameters. Generates a unique identifier (entry number) for
     * each contact. Gender is a string “Male”, or “Female”. Date of birth is a
     * number representing yyyymmdd. For example July 8, 2011 would be
     * represented by the number 20110708.
     */
    public Contact() {

    }

}
