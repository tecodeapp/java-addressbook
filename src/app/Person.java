package app;

/**
 * Class: Person
 * Description: Manages person data.
 *
 * Constructor The constructor accepts first name, last name, gender, and date of
 * birth as parameters and sets the relevant attributes accordingly. Date
 * of birth is specified as a number (yyyymmdd). For example, July 8,2011
 * would be the number 20110708
 *
 * Method Description
 * getName Returns name in the format “John Brown”.
 * getGender Returns gender as a string “Male”, or “Female”
 * getDOB Returns date of birth as a number (yyyymmdd).
 * For example, July 8,2011 would be the number 20110708
 */
public class Person {

    private Name name;
    private long dateOfBirth;
    private Gender gender;

    enum Gender {
        MALE,
        FEMALE
    };
    public Person(String firstName, String lastName, Gender gender, long dateOfBirth) {
        this.name = new Name(firstName, lastName);
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        // if (gender.equals(Gender.MALE)) {
        //     this.gender = gender;
        // } else if (gender.equals(Gender.FEMALE)) {
        //     this.gender = gender;
        // }
    }

    public String getName() {
        return name.getFirstName() + name.getLastName();
    }

    public Gender getGender() {
        return gender;
    }

    public long getDOB() {
        return dateOfBirth;
    }

    public static void main(String[] args)  {
        System.out.println("Hello Java");
    }
}

